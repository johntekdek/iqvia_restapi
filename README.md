# IQVIA_RESTapi
Contact Management REST Api.

# Install
```
git clone https://johntekdek@bitbucket.org/johntekdek/iqvia_restapi.git
virtualenv env
activate virtaulenv
pip install -r requirements.txt
python run.py
```

# Test
```
Pytest -v
```

# End Points


```
    EndPoint             HTTP Method         CRUD Method         Result
   /contacts            GET                 READ                get all contacts
   
```


# Celery
* Assumes Redis is running.
```
celery beat -A hays_1.main.tasks
celery worker -A hays_1.main.tasks
```
